*** Settings ***
Documentation       Referente aos auxiliares da automação

Resource            ../base.robot

*** Keywords ***
Swipe Ate Elemento Visivel
  [Arguments]   ${element}
  FOR   ${i}    IN RANGE    15
    ${Result}=    Run KeyWord And Return Status       Element Should Be Enabled        ${element}
    ${Result2}=    Run KeyWord And Return Status       Page Should Contain Element        ${element}
    Exit For Loop If    ${Result}
    Exit For Loop If    ${Result2}
    Swipe   200    600    200    100    
  END

Swipe Ate Texto Visivel
  [Arguments]                                 ${text}
  FOR   ${i}    IN RANGE    15
    ${Result}=    Run KeyWord And Return Status       Page Should Contain Text        ${text}
    Set Suite Variable                ${result_exists}               ${Result}
    Exit For Loop If    ${Result}
    Swipe   200    600    200    100    
  END

E toco no botao
  [Arguments]       ${texto_botao}
  Click Text     ${texto_botao}

Então devo visualizar a mensagem
    [Arguments]         ${mensagem}
    Wait Until Page Contains                ${mensagem}
    Page Should Contain Text                ${mensagem}
