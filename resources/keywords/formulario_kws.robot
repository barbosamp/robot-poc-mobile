*** Settings ***

Resource            ../base.robot

*** Keywords ***

Quando criar um novo formulario
  Wait Until Page Contains Element       ${Formulario.input_name}
  Set Suite Variable             ${name}                  Marcos Barbosa
  Input Text                     ${Formulario.input_name}     ${name}
  Click Element                  ${Formulario.checkbox}
  E toco no botao
  ...         SALVAR 

Então devo visualizar os dados exibidos na tela
  Wait Until Page Contains Element      xpath=//android.widget.TextView[@text='Nome: ${name}']
  Page Should Contain Element           xpath=//android.widget.TextView[@text='Nome: ${name}']