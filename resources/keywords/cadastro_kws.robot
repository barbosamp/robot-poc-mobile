*** Settings ***

Resource            ../base.robot

*** Keywords ***
Quando realizo cadastro com dados validos
    Wait Until Page Contains Element        ${Login.botao_novo_usuario}
    Click Element                           ${Login.botao_novo_usuario}
    Wait Until Page Contains Element        ${Cadastro.input_nome}
    Input Text                              ${Cadastro.input_nome}      Automação
    ${random}                               Generate random string    4    0123456789
    Set Test Variable                       ${email}        ${random}@qatest.com.br
    Input Text                              ${Cadastro.input_email}          ${email}
    Input Text                              ${Cadastro.input_senha}          teste123
    Click Element                           ${Cadastro.botao_cadastrar}