*** Settings ***
# Libraries
Library     AppiumLibrary
Library     Process
Library     FakerLibrary    locale=pt_BR
# Library    libs/imap.py
Library     String
Library     Collections
Library     OperatingSystem

# Keywords

Resource        keywords/home_kws.robot
Resource        keywords/helper_kws.robot
Resource        keywords/formulario_kws.robot
Resource        keywords/cadastro_kws.robot


# Pages

Resource        pages/${PLATFORM}/home_elements.robot
Resource        pages/${PLATFORM}/login_elements.robot
Resource        pages/${PLATFORM}/cadastro_elements.robot
Resource        pages/${PLATFORM}/formulario_elements.robot


# Data

Variables        data/credentials.yaml
Variables        data/messages.yaml
Variables        data/search.yaml

# Hooks

Resource        hooks.robot