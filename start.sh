#!/bin/bash

# clean previous output files
rm -f logs/output.xml
rm -f logs/rerun.xml
rm -f logs/first_run_log.html
rm -f logs/second_run_log.html


helpFunction()
{
   echo ""
   echo "Usage: $0 server tag teste"
   echo ""
   echo -e "\tserver   Servidor de execução. --------- [Local - Executa testes em sua máquina local.]"
   echo -e "\t                                         [Browser_Stack - Define servidor de execução remoto no Browser Stack]"
   echo -e "\t                                         [Docker - Define servidor de execução remoto no docker local]"
   echo -e "\tplataforma       Plataforma para execução. ----- [Android/iOS - Define em qual plataforma os testes serão executados]"
   echo -e "\ttag      Tag para execução dos testes. - [SMOKE - Executa somente testes principais do app.]"
   echo -e "\t                                         [REGRESSIVO - Executa todos os cenários regresivos do app.]"
   echo -e "\tteste    Caminho do cenário de teste. -- [Ex: features/compra.robot]"
   echo -e "\tbs_user  Usuário utilizado no Browser Stack -- [Ex: claas12]"
   echo -e "\tbs_key   Chave do usuário utilizado no Browser Stack -- [Ex: VqyauwKsvMzBdasqPNDUyn]"
   echo -e "\tbs_url   Url de conexão do app no Browser Stack -- [Ex: bs://a3877c27c01341sa6a790846224902c1307cd5clare54]"
   echo -e "\t"
   exit 1 # Exit script after printing help
}

server_name="$(echo ${1} | tr 'A-Z' 'a-z')"
platform_name="$(echo ${2} | tr 'A-Z' 'a-z')"
tag_name=${3}
test_name=${4}
bs_user=${5}
bs_key="$(echo ${6} | tr 'A-Z' 'a-z')"
bs_url=${7}
allure="allure_robotframework;logs/allure"

echo
echo "#######################################"
echo "#     Executando suíte de testes.     #"
echo "#######################################"
echo

# Allure Report generate
allure_generate_report()
{
   echo
   echo "#######################################################"
   echo "# Construindo report de execução com AllureFramework. #"
   echo "#######################################################"
   echo

   allure generate logs/allure -o logs/allure-report

   # => Move screenshot to allure-report.
   cp logs/*.png logs/allure-report/data/attachments

   # Allure open go to open a new browser tab with report.
   echo
   echo "#######################################################"
   echo "# Gerando report AllureFramework. #"
   echo "#######################################################"
   echo
   # => Allure open a tab browser with report.
   allure open logs/allure-report
}

# Merge the rerun report files
merge_reports()
{
   cp logs/log.html  logs/second_run_log.html

   echo
   echo "##########################################"
   echo "# Mesclando arquivos de log de execução. #"
   echo "##########################################"
   echo
   rebot --nostatusrc --output logs/output.xml --merge logs/output.xml  logs/rerun.xml
}

merge_reports_allure()
{
   cp logs/log.html  logs/second_run_log.html

   echo
   echo "##########################################"
   echo "# Mesclando arquivos de log de execução. #"
   echo "##########################################"
   echo
   rebot --nostatusrc --output logs/output.xml --merge logs/output.xml  logs/rerun.xml
   rebot --nostatusrc --output logs/allure/output.xml --merge logs/allure/output.xml logs/output.xml
      # => Robot Framework generates a new output.xml
}

# Validates whether re-execution is required
# Stop the script here if all the tests were OK
status()
{
   if [ $? -eq 0 ]; then
      echo "Não foram encontradas falhas em casos de teste durante a execução. Sua suíte de testes está OK."
      return 0
   else 
      return 1
   fi
}

rerun_android_local()
{
   echo
   echo "##############################################################"
   echo "# Reexecução dos casos de testes que falharam anteriormente. #"
   echo "##############################################################"
   echo
   robot --outputdir logs/ --xunit resultado2.xml --nostatusrc --rerunfailed logs/output.xml --output rerun.xml -e NOK -i $tag_name -v SERVER:$server_name -v PLATFORM:$platform_name ${test_name}
   # => Robot Framework generates file rerun.xml
}

# Run the tests in the Local workspace with emulator, if you find any failure, perform a re-execution of the test scenarios that failed.
android_local()
{
   robot --listener $allure  --outputdir logs/ --xunit resultado.xml -v SERVER:$server_name -v PLATFORM:$platform_name -e NOK -i $tag_name ${test_name}
   status
   RESULT=$?

   if [ "$RESULT" -eq "1" ]; then
      rerun_android_local
      merge_reports_allure
   fi

   allure_generate_report
}

rerun_docker_ci()
{
   echo
   echo "##############################################################"
   echo "# Reexecução dos casos de testes que falharam anteriormente. #"
   echo "##############################################################"
   echo
   robot --outputdir logs/ --xunit resultado2.xml --nostatusrc --rerunfailed output.xml --output rerun.xml -e NOK -i $tag_name --variable SERVER:$server_name -v PLATFORM:$platform_name ${test_name}
   # => Robot Framework generates file rerun.xml
}

# Run the tests in the AWS Server EC2 with emulator, if you find any failure, perform a re-execution of the test scenarios that failed.
docker_ci()
{
   robot --outputdir logs/ --xunit resultado.xml -v SERVER:$server_name -v PLATFORM:$platform_name -e NOK -i $tag_name ${test_name}
   status
   RESULT=$?

   if [ "$RESULT" -eq "1" ]; then
      rerun_docker_ci
      merge_reports
   fi
   exit 0
}

# Run the tests in the Browser Stack, if you find any failure, perform a re-execution of the test scenarios that failed.
browser_stack()
{
   robot --outputdir logs/ --xunit resultado.xml -v SERVER:$server_name -v PLATFORM:$platform_name -v BS_USER:$bs_user -v BS_KEY:$bs_key -v APP_URL:$bs_url -i $tag_name ${test_name}
   status
   RESULT=$?

   if [ "$RESULT" -eq "1" ]; then
      rerun_browser_stack
      merge_reports
   fi
   exit 0
}

rerun_browser_stack()
{
   # we launch the tests that failed
   echo
   echo "##############################################################"
   echo "# Reexecução dos casos de testes que falharam anteriormente. #"
   echo "##############################################################"
   echo
   robot --outputdir logs/  --nostatusrc --rerunfailed logs/output.xml --output rerun.xml --xunit resultado2.xml -v SERVER:$server_name -v PLATFORM:$platform_name -v BS_USER:$bs_user -v BS_KEY:$bs_key -v APP_URL:$bs_url -i $tag_name ${test_name}
   # => Robot Framework generates file rerun.xml
}

if [ "${test_name}" != "features/" ]; then
   test_name="${test_name}".robot
fi

while :
do
  case $server_name in
	"browser_stack")
		browser_stack
		;;
	"docker")
		docker_ci
		break
		;;
   "local")
      android_local
      break
   ;;
	*)
		echo "Invalid parameter value!: "$server_name" - Ex: Browser_Stack, Docker, Local"
      exit 0
		;;
  esac
done