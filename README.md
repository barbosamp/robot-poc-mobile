## #####################################################################
## 																	  ##
##                        Automação App Android e iOS 	
---

## 
Para executar a automação de testes pelo Gitlab, acesse:
[GitLab Schedules](url_gitlab_schedules) 

## APK utilizado
	- https://github.com/brunobatista25/best_archer/tree/master/tests/apks

### Variáveis de execução.
As variáveis de execução podem ser configuradas dentro de qualquer schedule.

|        Nome     |     Função  | Valores  | 
| --------------  | ----------- | -------- |
|    FEATURE_NAME |   Define a feature que será executada.  | Analisar coluna de Funcionalidades. |
|    TAG_NAME     |   Define tag que será executada.  | Analisar coluna de Tags.            |
|    SERVER_NAME  |   Servidor de execução onde os testes serão executados.  | Browser_Stack, Docker |  
|    BS_USER      |   Usuário do Browser Stack para execução.   | Ex: claire4 |
|    BS_KEY       |   Chave do usuário do Browser Stack.   | Ex: vbnsdaiu54sa3213ss |
|    APP_URL      |   Url do browser stack contendo o app que será executado os testes. | Ex: bs://dasuhsaugdashdda2382iqwicna |
|    PLATAFORMA   |   Plataforma que serão executados os testes.      | android, ios |


### Lista de features e tags disponíveis para execução. 
* Para executar todas as features, basta deixar o valor do campo FEATURE_NAME vazio.

| Funcionalidade  |     Tag     | 
| --------------  | ----------- |
|    busca        |    BUSCA    |
|   cadastro      |   CADASTRO  |
|    compra       |    COMPRA   |



|                Descrição                   |     Tag         | 
| ------------------------------------------ | --------------- |
|   Executa todos os testes regressivos.     |   REGRESSIVO    |
|   Executa todos os smoke tests.            |   SMOKE         |
|   * Executa um teste específico.           |   TEDXAA-XX     |

 - \*Acesse os testes disponíveis [aqui](url_trello/jira_board)! 
---  
---
### Para configurar o Ambiente Windows, acesso o link abaixo:

		- [Confira aqui!](https://medium.com/@lorainegarutti/automa%C3%A7%C3%A3o-de-testes-mobile-configurando-ambiente-robot-appiumlibrary-4e759c11dbc7)
		

### Para configurar o Ambiente Mac OS, acesse o link abaixo:

		- [Confira aqui!](https://medium.com/@laedyceci/configurando-appium-no-macos-para-automa%C3%A7%C3%A3o-mobile-em-android-c9bfb23cd36b)

### Para conhecer os comandos basicos do Git acesse o link:

		- [Confira aqui!](https://www.hostinger.com.br/tutoriais/comandos-basicos-de-git)

## #####################################################################
## 																	  ##
##                   BOAS PRÁTICAS UTILIZADAS						  ##
##                                            						  ##
## #####################################################################
* [Confira aqui!](https://github.com/robotframework/HowToWriteGoodTestCases/blob/master/HowToWriteGoodTestCases.rst#test-suite-names)

### Abrir o terminal e executar os seguintes passos:
## #####################################################################
## 																	  ##
##                   CONFIGURAÇÃO ADICIONAL - MAC OS				  ##
##                                            						  ##
## #####################################################################

### Abrir o terminal e executar os seguintes passos:

* Instalar homebrew

		$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


## #####################################################################
## 																	  ##
##                        ESTRUTURA DO PROJETO  					  ##
##                                            						  ##
## #####################################################################

* No projeto, temos a seguinte estrutura de pastas:

	- app
		- android
			- homolog
			- prod
		- ios
			- homolog
			- prod
	- features
	- logs
	- resources
		- data
		- keywords
		- pages
		- libs

* Segue descrição das pastas que fazem parte do projeto:

	- app: Nesta pasta ficarão presentes todos os .apks que serão utilizados no nosso teste.
	- logs: Pasta responsável por armazenar as evidências e relatórios pós execução.
	- resources: Na pasta resources, teremos todos os recursos como keywords, pages e libs que serão utilizadas no projeto.
	- keywords: Aqui estarão todas as keywords criadas no projeto separadas por features.
	- pages: Na pasta pages estarão todos os pages objects mapeados por tela.
	- libs: A pasta libs contém todas as libs e implementações externas que poderão ser aplicadas e utilizadas em nossos testes.
	- tests: Pasta responsável por amazenarem todos os BDD's e steps dos nossos testes.


## #####################################################################
## 																	  ##
##                CAPABILITIES E CONFIGURAÇÕES GERAIS  			      ##
##                                            						  ##
## #####################################################################

* Onde estão os capabilities do Appium:

	- resources
		- hooks.robot

* Todos os capabilities do Appium, estão presentes dentro da pasta /resources no arquivo hooks.robot
* Qualquer ajuste de capabilitie deverá ser feito apenas no hooks.robot

* Temos um arquivo chamado base.robot que se encontra na seguinte hierarquia:

	- resources
		- base.robot

* Esse arquivo serve para concentrar todas as Libraries e Rosources que serão importados e utilizados no projeto.
* Sendo assim, qualquer recurso ou Library nova, devem ser passandos apenas neste arquivo.


## #####################################################################
## 																	  ##
##                        EXECUTANDO OS TESTES  					  ##
##                                            						  ##
## #####################################################################

* Para executarmos os testes, precisamos abrir o terminal integrado na pasta raiz do projeto.*
* Neste caso a raiz é a pasta automacao-app. 
* Para executarmos todos os testes existentes no projeto, devemos executar o seguinte comando:

	- $ robot -d ./logs tests/

* Já para executarmos uma feature específica, devemos informar qual .robot querermos executar.
* Neste caso, vamos executar os testes de login como Exemplo:

	- $ robot -d ./logs tests/login.robot

* Também podemos executar cenários específicos, desde que os mesmos possuam sua Tag única. 
* Como exemplo, podemos utilizar os testes de account. Cada cenário possui sua Tag unica e neste caso vamos executar o cenário de Inclusão de endereço.
* Para isso, inserímos o comando -i e logo a frente a tag que desejamos executar. Veja no exemplo:

	- $ robot -d ./logs -i Incluir tests/account.robot

* Caso queira alterar o locals onde os logs são salvos, basta ajustar o caminho após o -d.
* Exemplo: Iremos passar no comando que os logs/reports serão salvos na pasta reports:

	- $ robot -d ./reports tests/login.robot
