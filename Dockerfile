FROM python:3

WORKDIR /usr/src/

COPY . ./
COPY ./logs/report.zip ./
RUN pip install requests

CMD [ "robot", "-d logs features" ]