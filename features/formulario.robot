*** Settings ***
Documentation     Escreva cenários curtos e independentes.
...    Faça uso das Tags para controle de execução

Resource    ../resources/base.robot

Test Setup        Open Session ${PLATFORM} ${SERVER}
Test Teardown     Close Session

*** Test Case ***
Cenario: Adicionar Formulario 
  [Tags]    3940    ANDROID   FORMULARIO   REGRESSIVO
  Dado que estou na aplicação
  E toco no botao
  ...         Formulário
  Quando criar um novo formulario
  Então devo visualizar os dados exibidos na tela
