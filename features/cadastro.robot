*** Settings ***
Documentation     Escreva cenários curtos e independentes.
...    Faça uso das Tags para controle de execução

Resource    ../resources/base.robot

Test Setup        Open Session ${PLATFORM} ${SERVER}
Test Teardown     Close Session

*** Test Case ***
Cenario: Cadastro com Sucesso
  [Tags]    ANDROID   LOGIN   REGRESSIVO
  Dado que estou na aplicação
  E toco no botao  SeuBarriga Híbrido
  Quando realizo cadastro com dados validos
  Então devo visualizar a mensagem    Usuário inserido com sucesso